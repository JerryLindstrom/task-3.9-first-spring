package com.example.Assignmentspringinitialzr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignmentSpringInitialzrApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssignmentSpringInitialzrApplication.class, args);
    }

}
