package com.example.Assignmentspringinitialzr.web;

import org.springframework.web.bind.annotation.*;

@RestController
public class Web {

    @GetMapping("/")
    public String index() {
        return "Hello, Welcome to this site!";
    }

    @GetMapping("/greeting")
    public String greeting() {
        String name = "Jerry";
        return "Welcome " + name + " to this simple Spring homepage.";
    }

    @GetMapping("/greeting/{name}")
    public String greetingName(@PathVariable String name) {
        return "Welcome " + name + " to this simple Spring homepage.";
    }

    //@RequestMapping(value = "/palindrome", method = RequestMethod.GET)
    @GetMapping("/palindrome")
    public String echoQuery(@RequestParam("query") String searchTerm) {
        String reverse = "";
        int length = searchTerm.length();
        for ( int i = length - 1; i >= 0; i-- )
            reverse = reverse + searchTerm.charAt(i);
        if (searchTerm.equals(reverse))
            return searchTerm + ", Is a palindrome.";
        else
            return searchTerm + ", Isn't a palindrome.";
    }
}





